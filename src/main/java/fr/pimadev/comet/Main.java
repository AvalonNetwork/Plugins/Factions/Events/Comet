package fr.pimadev.comet;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class Main extends JavaPlugin {

    public final String PREFIX         = "§6";
    public final String SCHEMATIC_NAME = "comet.schematic";

    @Getter
    private List<Location> cometLocations;
    @Getter
    @Setter
    private Event          event;
    @Getter
    private int blockToBreakID;
    @Getter
    private int itemToDropID;
    @Getter
    private int blocksToBreak;
    @Getter
    private int hitsNeeded;

    @Override
    public void onEnable() {
        super.onEnable();
        this.loadConfig();
        this.getCommand("comet").setExecutor(new CometCommand(this));
        this.getServer().getPluginManager().registerEvents(new EventsListener(this), this);
        this.cometLocations = this.parseLocations(this.getConfig().getStringList("comet_list"));
        this.blockToBreakID = this.getConfig().getInt("block_to_break_id");
        this.itemToDropID = this.getConfig().getInt("item_to_drop_id");
        this.blocksToBreak = this.getConfig().getInt("total_of_blocks_to_break_in_schematic");
        this.hitsNeeded = this.getConfig().getInt("arrow_hits_number");
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    private void loadConfig() {
        File config = new File(this.getConfig().getCurrentPath());
        if (!config.exists()) {
            this.getConfig().options().copyDefaults(true);
            this.saveConfig();
        }
    }

    private Location parseLoc(String serializedLocation) {
        String[] str   = serializedLocation.split(",");
        String   world = str[0];
        double   x     = Double.valueOf(str[1]).doubleValue();
        double   y     = Double.valueOf(str[2]).doubleValue();
        double   z     = Double.valueOf(str[3]).doubleValue();
        return new Location(Bukkit.getWorld(world), x, y, z);
    }

    private List<Location> parseLocations(List<String> serializedLocations) {
        List<Location> locations = new ArrayList<>();
        for (String serializedLoc : serializedLocations) {
            Location location = this.parseLoc(serializedLoc);
            if (!locations.contains(location)) {
                locations.add(location);
            }
        }
        return locations;
    }

}
