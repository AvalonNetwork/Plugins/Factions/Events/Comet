package fr.pimadev.comet;

import fr.pimadev.timerapi.Timer;
import fr.pimadev.timerapi.TimerListener;
import fr.pimadev.worldeditapi.Schematic;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Event {

    private Main                   main;
    private Schematic              schematic;
    private Location               cometLocation;
    private Timer                  timer;
    @Getter
    private Map<Location, Integer> hitsBlocks = new HashMap<>();
    private int                    blocksLeft;

    public Event(Main main) {
        this.main = main;
        this.cometLocation = this.main.getCometLocations().get(new Random().nextInt(this.main.getCometLocations().size()));
        this.blocksLeft = this.main.getBlocksToBreak();
    }

    public void preStart() {
        this.timer = new Timer(this.main, 0, 10, this.main.PREFIX + "L'event commence dans §e§l " + Timer.LEFT_TIME + " §l! Coordonnées de la comète : X: §e§l" + this.cometLocation.getBlockX() + " §6Y: §e§l" + this.cometLocation.getBlockY() + " §6Z: §e§l" + this.cometLocation.getBlockZ(), new TimerListener() {
            @Override
            public void onFinish() {
                Event.this.start();
            }
        });
    }

    public void start() {
        Bukkit.broadcastMessage(this.main.PREFIX + "L'event commence !");
        this.schematic = new Schematic(this.main, this.main.SCHEMATIC_NAME);
        this.schematic.paste(this.cometLocation);
    }

    public void stop() {
        if (this.timer != null) {
            this.timer.stop();
        }
        if (this.schematic != null) {
            this.schematic.undoPaste();
        }
        this.main.setEvent(null);
    }

    public void blockHitByArrow(Location location) {
        Integer hits = this.hitsBlocks.getOrDefault(location, 0);
        if (hits <= this.main.getBlockToBreakID()) {
            hits++;
            this.hitsBlocks.put(location, hits);
        }
        if (hits == this.main.getHitsNeeded()) {
            location.getBlock().setType(Material.AIR);
            location.getWorld().dropItemNaturally(location, new ItemStack(Material.getMaterial(this.main.getItemToDropID())));
            this.blocksLeft--;
            if(this.blocksLeft <= 0) {
                Bukkit.broadcastMessage(this.main.PREFIX + "L'event est terminé !");
                this.stop();
            }
            else {
                Bukkit.broadcastMessage(this.main.PREFIX + "Il reste §e§l" + this.blocksLeft + " §6blocs.");
            }
        }
    }

}
