package fr.pimadev.comet;

import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

import java.lang.reflect.Field;

@AllArgsConstructor
public class EventsListener implements Listener {

    private Main main;

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent e) {
        if (this.main.getEvent() != null && e.getEntity() != null && (e.getEntity() instanceof Arrow)) {
            // Must be run in a delayed task otherwise it won't be able to find the block
            Bukkit.getScheduler().scheduleSyncDelayedTask(this.main, () -> {
                try {
                    net.minecraft.server.v1_7_R4.EntityArrow entityArrow = ((CraftArrow) e.getEntity()).getHandle();
                    Field fieldX = net.minecraft.server.v1_7_R4.EntityArrow.class.getDeclaredField("d");
                    Field fieldY = net.minecraft.server.v1_7_R4.EntityArrow.class.getDeclaredField("e");
                    Field fieldZ = net.minecraft.server.v1_7_R4.EntityArrow.class.getDeclaredField("f");

                    fieldX.setAccessible(true);
                    fieldY.setAccessible(true);
                    fieldZ.setAccessible(true);

                    int x = fieldX.getInt(entityArrow);
                    int y = fieldY.getInt(entityArrow);
                    int z = fieldZ.getInt(entityArrow);

                    if (y != -1) {
                        Block block = e.getEntity().getWorld().getBlockAt(x, y, z);
                        this.onArrowHitBlock(block);
                    }
                }
                catch (NoSuchFieldException e1) {
                    e1.printStackTrace();
                }
                catch (SecurityException e1) {
                    e1.printStackTrace();
                }
                catch (IllegalArgumentException e1) {
                    e1.printStackTrace();
                }
                catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                }
            });
        }
    }

    public void onArrowHitBlock(Block block) {
        if (block.getType().getId() == this.main.getBlockToBreakID()) {
            this.main.getEvent().blockHitByArrow(block.getLocation());
        }
    }

}
