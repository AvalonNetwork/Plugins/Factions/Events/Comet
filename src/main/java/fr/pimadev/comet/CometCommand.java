package fr.pimadev.comet;

import com.sk89q.worldedit.EmptyClipboardException;
import com.sk89q.worldedit.world.DataException;
import fr.pimadev.worldeditapi.Schematic;
import lombok.AllArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.IOException;

@AllArgsConstructor
public class CometCommand implements CommandExecutor {

    private Main main;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("§cVous devez être un joueur pour executer cette commande.");
            return true;
        }
        Player p = (Player) sender;
        if (!p.isOp()) {
            p.sendMessage("§cVous n'avez pas la permission d'executer cette commande.");
            return true;
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("define_comet")) {
                Schematic schematic = new Schematic(this.main, this.main.SCHEMATIC_NAME);
                try {
                    schematic.save(p);
                }
                catch (EmptyClipboardException e) {
                    p.sendMessage("§cVous devez mettre la schematic dans le presse papier (commande //copy)).");
                    return true;
                }
                catch (IOException e) {
                    p.sendMessage("§cErreur, consultez les logs.");
                    e.printStackTrace();
                    return true;
                }
                catch (DataException e) {
                    p.sendMessage("§cErreur, consultez les logs.");
                    e.printStackTrace();
                    return true;
                }
                p.sendMessage(this.main.PREFIX + "Vous avez défini la schematic de la comète.");
            }
            else if (args[0].equalsIgnoreCase("start")) {
                if (this.main.getEvent() != null) {
                    p.sendMessage(this.main.PREFIX + "§cIl y a déjà un event en cours.");
                    return true;
                }
                this.main.setEvent(new Event(this.main));
                this.main.getEvent().preStart();
            }
            else if (args[0].equalsIgnoreCase("stop")) {
                if (this.main.getEvent() == null) {
                    p.sendMessage(this.main.PREFIX + "§cIl n'y a pas d'event en cours.");
                    return true;
                }
                Bukkit.broadcastMessage(this.main.PREFIX + "§cL'event vient d'être stoppé par un administrateur.");
                this.main.getEvent().stop();
            }
        }
        return true;
    }
}
